
CREATE DATABASE SQLAssignment
USE SQLAssignment


CREATE TABLE Producers (Id int identity primary key,Name varchar(100) ,sex varchar(10), DOB date, Bio varchar(0250));
CREATE TABLE Actors (Id int identity primary key,Name varchar(100) ,sex varchar(10), DOB date, Bio varchar(250));
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) , YearOfRelease int, Plot varchar(250),ProducersId int references Producers(Id));
CREATE TABLE MovieActorMapping (Id int identity primary key,MoviesId int references Movies(id),ActorsId int references Actors(id));
CREATE TABLE Genres(Id int identity primary key,Name varchar(100));
CREATE TABLE MovieGenreMapping(Id int identity primary key, MoviesId int references Movies(id),GenresId int references Genres(id));




INSERT INTO Producers VALUES ('Yash Chopra','Male','05/14/1960','Labeled the eternal romantic and with one of the best musical senses in the business, Yash Chopra is arguably India''s most successful director of romantic films.')
INSERT INTO Producers VALUES ('Aditya Chopra','Male','09/11/1971','2. Aditya Chopra Producer | Mohabbatein The elder son of veteran filmmaker, Yash Chopra & Pamela Chopra, Aditya was born in the year 1971. Since childhood, he was an ardent film buff')
INSERT INTO Producers VALUES ('Zoya Akhtar','Female','11/03/1987','Zoya Akhtar hails from one of the most talented film families in the country. After completing a diploma in filmmaking from NYU, she assisted directors like Mira Nair, Tony Gerber, and Dev Benegal, before turning Writer-Director in her own right.')
INSERT INTO Producers VALUES ('Farhan Akhtar','Male','11/14/1974','An accomplished and award-winning producer-director-writer-actor-singer, Farhan Akhtar is the ultimate Bollywood multi-hyphenate. Born in 1974 to prolific Indian film writer and poet, Javed Akhtar and writer-director, Honey Irani')




INSERT INTO Actors VALUES ('Mila Kunis','Female','11/14/1986','Milena Markovna Kunis is an American actress. In 1991, at the age of 7, she moved from Soviet Ukraine to the United States with her family')
INSERT INTO Actors VALUES ('Robert DeNiro','Male','07/10/1957','Robert Anthony De Niro Jr is an American actor, producer, and director who holds both American and Italian citizenship')
INSERT INTO Actors VALUES ('George Michael','Male','11/23/1978','George Michael was an English singer, songwriter, record producer, and philanthropist who rose to fame as a member of the music duo Wham! and later embarked on a solo career')
INSERT INTO Actors VALUES ('Mike Scott','Male','08/06/1969','James Michael Scott is an American professional basketball player for the Philadelphia 76ers of the National Basketball Association.')
INSERT INTO Actors VALUES ('Pam Halpert','Female','09/26/1996','Pamela Morgan Halpert is a fictional character on the U.S. television sitcom The Office, played by Jenna Fischer. ')




INSERT INTO Movies VALUES ('Rocky',2006,'Now long-retired, Rocky (Sylvester Stallone) runs a Philadelphia eatery and mourns the loss of his beloved wife, Adrian. Yearning to recapture a bit of his glory days, he plans to re-enter the ring for a few low-profile, local matches',1)
INSERT INTO Movies VALUES ('Rocky',1981,'Post his father''s death, Rakesh is traumatised and, thus, adopted by a Christian couple who rename him, Rocky. However, he later realises his true identity and vows to avenge his father''s murder.',2)
INSERT INTO Movies VALUES ('Terminal',2004,'When Viktor Navorski (Tom Hanks), an Eastern European tourist, arrives at JFK in New York, war breaks out in his country and he finds himself caught up in international politics.',1)
INSERT INTO Movies VALUES ('Rambo',1982,'The film is based on the 1972 novel of the same name by David Morrell. In the film, Rambo, a troubled and misunderstood veteran, must rely on his combat and survival senses against the abusive law enforcement of the small town of Hope, Washington',3)
INSERT INTO Movies VALUES ('Rambo',2020,'Rambo is the Bollywood remake of the Sylvester Stallone starrer Hollywood blockbuster series, the film features Tiger Shroff in the titular role. Packed with high octane action, Rambo will tell the story of an army man with high survival skills.',3)



INSERT INTO MovieActorMapping VALUES (1,1)
INSERT INTO MovieActorMapping VALUES (1,3)
INSERT INTO MovieActorMapping VALUES (1,4)
INSERT INTO MovieActorMapping VALUES (2,2)
INSERT INTO MovieActorMapping VALUES (3,4)
INSERT INTO MovieActorMapping VALUES (3,5)
INSERT INTO MovieActorMapping VALUES (3,2)
INSERT INTO MovieActorMapping VALUES (3,1)
INSERT INTO MovieActorMapping VALUES (4,1)
INSERT INTO MovieActorMapping VALUES (4,2)
INSERT INTO MovieActorMapping VALUES (5,1)
INSERT INTO MovieActorMapping VALUES (5,2)
INSERT INTO MovieActorMapping VALUES (5,3)
INSERT INTO MovieActorMapping VALUES (5,4)
INSERT INTO MovieActorMapping VALUES (5,5)



INSERT INTO Genres VALUES('Comedy')
INSERT INTO Genres VALUES('Action')
INSERT INTO Genres VALUES('Drama')
INSERT INTO Genres VALUES('Romance')
INSERT INTO Genres VALUES('Biography')




INSERT INTO MovieGenreMapping VALUES(1,2)
INSERT INTO MovieGenreMapping VALUES(1,3)
INSERT INTO MovieGenreMapping VALUES(2,1)
INSERT INTO MovieGenreMapping VALUES(2,3)
INSERT INTO MovieGenreMapping VALUES(2,4)
INSERT INTO MovieGenreMapping VALUES(3,2)
INSERT INTO MovieGenreMapping VALUES(3,3)
INSERT INTO MovieGenreMapping VALUES(4,5)
INSERT INTO MovieGenreMapping VALUES(5,1)
INSERT INTO MovieGenreMapping VALUES(5,4)

