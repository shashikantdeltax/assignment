CREATE DATABASE IMDBTest
USE IMDBTest


CREATE TABLE Producers (Id int identity primary key,Name varchar(100) ,Company varchar(100),CompanyEstDate date)
CREATE TABLE Actors (Id int identity primary key,Name varchar(100) ,Gender varchar(10),DOB date)
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) ,Language varchar(10),ProducerId int references Producers(Id),Profit decimal(18,6))
CREATE TABLE MovieActorMapping (Id int identity primary key,MovieId int references Movies(Id),ActorsId int references Actors(Id))


INSERT INTO Producers VALUES ('Arjun','Fox','05/14/1998')
INSERT INTO Producers VALUES ('Arun','Bull','09/11/2004')
INSERT INTO Producers VALUES ('Tom','Hanks','11/03/1987')
INSERT INTO Producers VALUES ('Zeshan','Chillie','11/14/1996')

INSERT INTO Actors VALUES ('Mila Kunis','Female','11/14/1986')
INSERT INTO Actors VALUES ('Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors VALUES ('George Michael','Male','11/23/1978')
INSERT INTO Actors VALUES ('Mike Scott','Male','08/06/1969')
INSERT INTO Actors VALUES ('Pam Halpert','Female','09/26/1996')

INSERT INTO Movies VALUES ('Rocky','English',1,10000)
INSERT INTO Movies VALUES ('Rocky','Hindi',2,3000)
INSERT INTO Movies VALUES ('Terminal','English',1,300000)
INSERT INTO Movies VALUES ('Rambo','English',3,93000)
INSERT INTO Movies VALUES ('Rambo','Hindi',3,82000)

INSERT INTO MovieActorMapping VALUES (1,1)
INSERT INTO MovieActorMapping VALUES (1,3)
INSERT INTO MovieActorMapping VALUES (1,4)
INSERT INTO MovieActorMapping VALUES (2,2)
INSERT INTO MovieActorMapping VALUES (3,4)
INSERT INTO MovieActorMapping VALUES (3,5)
INSERT INTO MovieActorMapping VALUES (3,2)
INSERT INTO MovieActorMapping VALUES (3,1)
INSERT INTO MovieActorMapping VALUES (4,1)
INSERT INTO MovieActorMapping VALUES (4,2)
INSERT INTO MovieActorMapping VALUES (5,1)
INSERT INTO MovieActorMapping VALUES (5,2)
INSERT INTO MovieActorMapping VALUES (5,3)
INSERT INTO MovieActorMapping VALUES (5,4)
INSERT INTO MovieActorMapping VALUES (5,5)





select * from MovieActorMapping
drop table Producers
drop table Actors
drop table Movies
drop table MovieActorMapping





--Update Profit of all the movies by +1000 where producer name contains 'run'
UPDATE Movies 
SET Profit = Profit+1000
WHERE ProducerId IN 
				(SELECT Id 
				FROM Producers 
				WHERE Name like '%run%')




--List down actors details which have acted in movies where producer name ends with 'n'
SELECT Actors.Id, Gender, DOB  FROM Actors 
INNER JOIN MovieActorMapping ON Actors.Id=ActorsId 
INNER JOIN Movies ON Movies.Id=MovieId 
INNER JOIN Producers ON Producers.Id=ProducerId
WHERE Producers.Name LIKE '%n'






--Find the avg age of male and female actors that were part of a movie called 'Terminal'  --Should return two rows
SELECT AVG(DATEDIFF(YY,DOB,GETDATE())) AS age, Gender  FROM Actors 
INNER JOIN MovieActorMapping ON Actors.Id=ActorsId 
INNER JOIN Movies ON Movies.Id=MovieId
WHERE Movies.Name='Terminal'
GROUP BY Gender





--Find the third oldest female actor 
SELECT Name, DATEDIFF(YY,DOB,GETDATE()) as oldest_female_age
FROM Actors 
WHERE DATEDIFF(YY,DOB,GETDATE()) IN 
				(SELECT TOP(3) DATEDIFF(YY,DOB,GETDATE()) 
                 FROM Actors where Gender='Female'
                 ORDER BY DATEDIFF(YY,DOB,GETDATE()) DESC)
ORDER BY DATEDIFF(YY,DOB,GETDATE())
OFFSET 2 ROWS





--List down top 3 profitable movies 
SELECT TOP 3 * FROM Movies 
ORDER BY Profit DESC






--List down the oldest actor and Movie Name for each movie
SELECT Movies.Name, MAX(DATEDIFF(YY,DOB,GETDATE())) as oldest_actor FROM Movies 
JOIN MovieActorMapping ON Movies.Id=MovieId 
JOIN Actors ON Actors.Id=ActorsId
GROUP BY Movies.Name,Movies.Language






--Find duplicate movies having the same name and their count
SELECT Name, COUNT(Name) 
FROM Movies
GROUP BY Name
HAVING COUNT(Name) > 1







--List down all the producers and +the movie name(even if they dont have a movie)
SELECT Producers.Name as Producer_Name, Movies.Name as Movie_Name 
FROM Producers LEFT JOIN Movies
ON Producers.Id=ProducerId